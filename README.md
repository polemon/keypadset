# keypadset

Setup tool for the PCsensor "PCsensor DIY-Keyboard PCBA": http://pcsensor.com/productd?product_id=757

The DIY-Keyboard PCBA acts as a HID keyboard once attached to a PC.
The device supports up to 14 push-button (momentary) switches.
Each one of those can be programmed to send a keystroke, or a sequence of keystrokes.

Common implementations are foot switches, but it can also be just a regular keypad.
